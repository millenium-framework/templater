<?php

namespace Millenium\Templater;

/**
 * Прослойка для шаблонизатора Fenom.
 *
 * @package     Millenium\Framework
 * @author      Alexandr Golikov <my-millenium@yandex.ru>
 * @copyright   2016 Alexandr Golikov
 */
class Fenom extends \Fenom
{

    /**
     * Trait для расширения возможностей Fenom.
     *
     * @var \Fenom\StorageTrait
     */
    use \Fenom\StorageTrait;

    /**
     * [$cacheDir description]
     *
     * @var [type]
     */
    private $cacheDir;

    /**
     * [setCacheDir description]
     *
     * @param [type] $dir [description]
     */
    public function setCachedDir($dir)
    {
        $this->cacheDir = $dir;

        return $this;
    }

    /**
     * Начальная инициализация Fenom.
     * Устанавливаем директории, опции, регистрируем свои фильтры, тэги и т.д.
     *
     * @return $this
     */
    public function init()
    {
        $this->setCompileDir($this->cacheDir);

        $options = [
            // Отключает возможность вызова методов в шаблоне
            'disable_methods' => false,
            // Отключает возможность использования фунций PHP, за исключением разрешенных
            'disable_native_funcs' => false,
            // Автоматически пересобирать кеш шаблона если шаблон изменился
            'auto_reload' => true,
            // Каждый раз пересобирать кеш шаблонов (рекоммендуется только для отладки)
            'force_compile' => true,
            // Не кешировать компилированный шаблон
            'disable_cache' => true,
            // Стараться по возможности вставить код дочернего шаблона в родительский при подключении шаблона
            'force_include' => true,
            // Автоматически экранировать HTML сущности при выводе переменных в шаблон
            'auto_escape' => false,
            // Автоматически проверять существование переменной перед использованием в шаблоне
            'force_verify' => true,
            // Отключает возможность вызова статических методов и функций в шаблоне
            'disable_php_calls' => true,
            // Удаляет лишиние пробелы в шаблоне
            'strip' => false
        ];

        $this->setOptions($options);

        $this->addModifier(
            'reset',
            function (array $array) {
                return reset($array);
            }
        );

        $this->addModifier(
            'rusLower',
            function ($string) {
                return mb_strtolower($string, 'UTF-8');
            }
        );

        $this->addModifier(
            'declOfNum',
            function ($number, $titles, $needNumber = true) {
                $cases = array (2, 0, 1, 1, 1, 2);

                $string = '';

                if ($needNumber) {
                    $string = $number . ' ';
                }

                if ($number % 100 > 4 && $number % 100 < 20) {
                    $case =  2;
                } else {
                     $case = min($number % 10, 5);
                }

                return $string . $titles[$case];
            }
        );

        return $this;
    }

    /**
     * Рендерит наш шаблон.
     *
     * @param string $template имя шаблона
     * @param array $variables массив с переменными для передачи в шаблон
     *
     * @return string готовый для отображения шаблон
     */
    public function render($template, array $variables = [])
    {
        $this->assign('pageContent', $this->fetch($template, $variables));

        return $this->fetch('main.tpl');
    }
}
