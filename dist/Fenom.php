<?php

namespace %namespace%;

use \Millenium\Framework\Registry;

class Fenom extends Registry\Di implements Registry\StorageInterface
{
    public function getClosure()
    {
         return function() {
             $templateDir = %get_templates_dir%;
             $cachedDir = %get_templates_cache_dir%;

             $fenom = new Millenium\Templater\Fenom(new \Fenom\Provider($templateDir));
             $fenom
                 ->setCachedDir($cachedDir)
                 ->init();

             return $fenom;
        };
    }
}
