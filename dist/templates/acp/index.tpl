<div class="container">
    <table>
        <tr>
            <td width="400px">
                <h1 class="content__title">
                    <b>Сайт</b>
                </h1>

                <div class="list-group">
                <h4 class="list-group-item-heading list-group-item active">
                    Пользователи - <span class="badge">{if !empty($count['users'])}{$count['users']}{else}0{/if}</span>
                </h4>
                    <a href="/admin/users" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Список пользователей
                        </h4>
                    </a>
                </div>

                <br />

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        Группы - <span class="badge">{if !empty($count['groups'])}{$count['groups']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/users/groups" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Список групп
                        </h4>
                     </a>
                </div>

                <br />

            </td>
            <td width="400px">

                <h1 class="content__title">
                    <b>База знаний</b>
                </h1>

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        Скриншоты - <span class="badge">{if !empty($count['screenshots'])}{$count['screenshots']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/screenshots" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Обработать скриншот.
                        </h4>
                    </a>
                </div>

                <br><br>------<br><br>

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        <a href="/admin/items" class="list-group-item">
                            Предметы
                        </a>
                        -
                        <span class="badge">{if !empty($count['items'])}{$count['items']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/items/create" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Добавить предмет
                        </h4>
                    </a>
                </div>

                <br />

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        <a href="/admin/quests" class="list-group-item">
                            Задания
                        </a>
                        -
                        <span class="badge">{if !empty($count['quests'])}{$count['quests']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/quests/create" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Добавить задание
                        </h4>
                    </a>
                </div>

                <br />

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        <a href="/admin/maps" class="list-group-item">
                            Карты
                        </a>
                        -
                        <span class="badge">{if !empty($count['maps'])}{$count['maps']}{else}0{/if}</span>
                    </h4>

                    <h4 class="list-group-item-heading">
                        <s>Добавить карту</s>
                    </h4>

                    {* <a href="/admin/maps/create" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Добавить карту
                        </h4>
                    </a> *}
                </div>

                <br />

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        <a href="/admin/npcs" class="list-group-item">
                            Персонажи
                        </a>
                        -
                        <span class="badge">{if !empty($count['npcs'])}{$count['npcs']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/npcs/create" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Добавить персонажа
                        </h4>
                    </a>
                </div>

                <br />

                <div class="list-group">
                    <h4 class="list-group-item-heading list-group-item active">
                        <a href="/admin/enemies" class="list-group-item">
                            Враги
                        </a>
                        -
                        <span class="badge">{if !empty($count['enemies'])}{$count['enemies']}{else}0{/if}</span>
                    </h4>

                    <a href="/admin/enemies/create" class="list-group-item">
                        <h4 class="list-group-item-heading">
                            Добавить врага
                        </h4>
                    </a>
                </div>
            </td>
        </tr>
    </table>
</div>
