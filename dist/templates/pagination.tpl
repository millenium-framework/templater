{if !empty($pagination) && $pagination['totalPages'] > 1}
    <ul
        class="pagination"
    >
        {if !empty($pagination['previousPage']) && $pagination['previousPage'] > 1}
            <li>
                <a
                    href="{$pagination['pageUrl']}1"
                    title="Первая"
                >
                    Первая
                </a>
            </li>
        {/if}

        {if $pagination['previousPage'] >= 1}
            <li>
                <a
                    href="{$pagination['pageUrl']}{$pagination['previousPage']}"
                    title="{$pagination['previousPage']}"
                >
                    {$pagination['previousPage']}
                </a>
            </li>
        {/if}

        <li class="active">
            <span>
                {$pagination['currentPage']}
            </span>
        </li>

        {if !empty($pagination['nextPage']) && $pagination['nextPage'] <= $pagination['totalPages']}
            <li>
                <a
                    href="{$pagination['pageUrl']}{$pagination['nextPage']}"
                    title="{$pagination['nextPage']}"
                >
                    {$pagination['nextPage']}
                </a>
            </li>
        {/if}

        {if $pagination['currentPage']+1 < $pagination['totalPages']}
            <li>
                <a
                    href="{$pagination['pageUrl']}{$pagination['totalPages']}"
                    title="Последняя"
                >
                    Последняя
                </a>
            </li>
        {/if}
    </ul>
{/if}
