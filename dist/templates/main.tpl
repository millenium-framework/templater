<!DOCTYPE html>
<html lang="en">
    <head>
        {include "head.tpl"}
    </head>

    <body>
        {include "header.tpl"}

        <div class="container">
            <div class="errors">
                {include "errors.tpl"}
            </div>

            <div class="content">
                {$pageContent}
            </div>

            <div class="pagination">
                {include "pagination.tpl"}
            </div>
        </div>

        {include "footer.tpl"}
    </body>
</html>
